<?php

namespace Kontentblocks\Common;


interface Exportable {

    public function export(&$collection);

}