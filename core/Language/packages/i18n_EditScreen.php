<?php

\Kontentblocks\Language\I18n::addPackage( 'EditScreen', array(
    // Common
    'notices' => array(
        'confirmDeleteMsg' => __( 'This action will erase all data. Are you sure?', 'Kontentblocks' ),
        'confirmDeleteYes' => __( 'Yes, delete', 'Kontentblocks'),
        'confirmDeleteNo' => __('No, keep module', 'Kontentblocks')
    )
) );
