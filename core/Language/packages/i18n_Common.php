<?php

\Kontentblocks\Language\I18n::addPackage(
    'Common',
    array(
        'title' => __( 'Title', 'kontentblocks' ),
        'action' => __('Action', 'kontentblocks'),
        'view' => __('View', 'kontentblocks'),
        'description' => __('Description', 'Kontentblocks'),
        'name' => __('Name', 'Kontentblocks'),
        'pageTemplates' => __('Page Templates', 'Kontentblocks'),

    )
);
