[![Build Status](https://travis-ci.org/kai-jacobsen/kontentblocks.svg?branch=master)](https://travis-ci.org/kai-jacobsen/kontentblocks) [![Code Climate](https://codeclimate.com/github/kai-jacobsen/kontentblocks/badges/gpa.svg)](https://codeclimate.com/github/kai-jacobsen/kontentblocks) [![Test Coverage](https://codeclimate.com/github/kai-jacobsen/kontentblocks/badges/coverage.svg)](https://codeclimate.com/github/kai-jacobsen/kontentblocks)

#Installation

##System Requirements

- PHP 5.5
- Composer

##Composer

require "jcbsn/kontentblocks" : "0.6.0

  
#Is this something for me?

If you:
- like to code and
- work on client and/or individual projects and
- face design constrains and
- need some flexibility when it comes to layout options.
- like to break up content into smaller parts and
- think that regular page builders add too much of everything.
- have some time to learn the concepts
- need a toolset which helps to quickly develop flexible sites

then this might be something for you.
And if you like it, I'd more than happy to hear your feedback, your ideas and criticism.

And if you are really interested, but don't know where or why to start..find me on twitter (@kjcbsn) and get in contact...
..the whole thing grew a bit too large for a single person, really.

[Cookbook](http://docs.kontentblocks.de/)

**Don't think about installing it if:**
- you except a clicky-di-click configuration ui
- drag and drop page building
- don't want to interact with code
- need something which does magic out of the box
- think yoda conditions sense they make
- don't like to ask questions because
- you expect a complete documentation (need help with that)

##Issues
yep, still some work to do...

##Stable
getting closer...